# Simple Login

## Requirements
- npm or yarn
- React Native CLI
- Android SDK setup, with emulator or device attached. See https://facebook.github.io/react-native/docs/getting-started.html

Should work on Windows, Linux or Mac.

## First steps

Install dependencies
```sh
$ npm install
```

Running on Android:
```sh
$ react-native run-android
```

Running on iOS:
```sh
$ react-native run-ios
```